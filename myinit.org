#+STARTIP: overview
* Interface tweaks

#+BEGIN_SRC emacs-lisp

(setq inhibit-startup-message t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "<f5>") 'revert-buffer)
#+END_SRC

#+RESULTS:
: revert-buffer

* try
#+BEGIN_SRC emacs-lisp

(use-package try
	:ensure t)
#+END_SRC

* whick key

#+BEGIN_SRC emacs-lisp 
(use-package which-key
	:ensure t
	:config
	(which-key-mode))
#+END_SRC

* org-bullets

#+BEGIN_SRC emacs-lisp

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

#+END_SRC

* Enabling Ibuffer instead of list buffer
#+BEGIN_SRC emacs-lisp
(defalias 'list-buffers 'ibuffer)
#+END_SRC
* ace-window

#+BEGIN_SRC emacs-lisp 
(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0))))) 
    ))
#+END_SRC
  
* counsel

#+BEGIN_SRC emacs-lisp 
(use-package counsel
  :ensure t
  )
#+END_SRC

* swiper

#+BEGIN_SRC emacs-lisp 
(use-package swiper
  :ensure try
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> l") 'counsel-load-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    ))
#+END_SRC

* avy

#+BEGIN_SRC emacs-lisp 
(use-package avy
  :ensure t
  :bind ("M-s" . avy-goto-char))

(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    ))
#+END_SRC

* color-theme

#+BEGIN_SRC emacs-lisp
(use-package color-theme
  :ensure t)
#+END_SRC

* zenburn-theme

#+BEGIN_SRC emacs-lisp

(use-package zenburn-theme
  :ensure t
  :config (load-theme 'zenburn t))
#+END_SRC

* Reveal js
#+BEGIN_SRC emacs-lisp
  (use-package ox-reveal
    :ensure t)
  (setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0")
  (setq org-reveal-mathjax t)
  (use-package htmlize
    :ensure t)
#+END_SRC
* flycheck
#+BEGIN_SRC emacs-lisp 
 (use-package flycheck
    :ensure t
    :init
    (global-flycheck-mode t))
#+END_SRC
* Jedi
#+BEGIN_SRC emacs-lisp
  (use-package jedi
    :ensure t
    :init
    (add-hook 'python-mode-hook 'jedi:setup)
    (add-hook 'python-mode-hook 'jedi:ac-setup))
#+END_SRC
* elpy
#+BEGIN_SRC emacs-lisp
  (use-package elpy
    :ensure t
    :config
    (elpy-enable))
#+END_SRC
* Web Mode
#+BEGIN_SRC emacs-lisp
  (use-package web-mode
    :ensure t
    :config
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (setq web-mode-engines-alist
	  '(("django" . "\\.html\\'")))
    (setq web-mode-ac-sources-alist
	  '(("css". (ac-source-css-property))
	    ("html". (ac-source-words-in-buffer ac-sources-abbrev))))
  (setq web-mode-enable-auto-closing t))
  
#+END_SRC
* Undo Tree
#+BEGIN_SRC emacs-lisp
  (use-package undo-tree
    :ensure t
    :init
    (global-undo-tree-mode))
#+END_SRC
* Misc Packages
#+BEGIN_SRC emacs-lisp
  (global-hl-line-mode t)
  (use-package hungry-delete
    :ensure t
    :config
    (global-hungry-delete-mode))

  (use-package expand-region
    :ensure t
    :config
    (global-set-key (kbd "C-=") 'er/expand-region))

  (use-package iedit
    :ensure t)

#+END_SRC
* Other small tweaks
#+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil)

#+END_SRC
